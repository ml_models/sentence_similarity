from sentence_transformers import SentenceTransformer, util
import gradio as gr

def similarity_score(sentences1,sentences2):
    model = SentenceTransformer(r'C:\Users\ganes\OneDrive\Documents\data_science\nata_nerve\project\model_creation')
    #Compute embedding for both lists
    embeddings1 = model.encode(sentences1, convert_to_tensor=True)
    embeddings2 = model.encode(sentences2, convert_to_tensor=True)
    #Compute cosine-similarities
    cosine_scores = util.cos_sim(embeddings1, embeddings2)
    return cosine_scores[0][0]

interface=gr.Interface(fn=similarity_score,
                      inputs=['text','text'],
                      outputs='text')

interface.launch()